package com.company;



interface Device{
    public void dolt();
}

 class Electronic implements Device{
    public void dolt() {

    }
}

abstract class Phone1 extends Electronic{

}

abstract class Phone2 extends Electronic{

    public void dolt(int x) {

    }

}

class Phone3 extends Electronic implements Device{

    public void doStuff(){

    }
}

/*

Answer is A.Compilation succeeds

 */
